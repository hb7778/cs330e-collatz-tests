#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self): 
        s = "3 30\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 3)
        self.assertEqual(j, 30)

    def test_read_3(self): 
        s = "119 461\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 119)
        self.assertEqual(j, 461)

    def test_read_3(self): 
        s = "81 101\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 81)
        self.assertEqual(j, 101)
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(500, 2500)
        self.assertEqual(v, 209)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
   
    def test_eval_5(self): 
        v = collatz_eval(10,1)
        self.assertEqual(v, 20)
    def test_evel_6(self): 
        v = collatz_eval(1,1000)
        self.assertEqual(v, 179)
        

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    def test_print_1(self): 
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")
    def test_print_2(self): 
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")
    def test_print_3(self): 
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")




    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    def test_solve_1(self):
        r = StringIO("8 17\n4 13\n25 447\n608 986\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "8 17 20\n4 13 20\n25 447 144\n608 986 179\n")
    def test_solve_2(self):
        r = StringIO("681 722\n1 1\n343 201\n51 83\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "681 722 171\n1 1 1\n343 201 144\n51 83 116\n")
    def test_solve_3(self):
        r = StringIO("3002 50024\n6 69\n1023 7845\n123 321\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3002 50024 324\n6 69 113\n1023 7845 262\n123 321 131\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()