#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "999999 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  999999)
        self.assertEqual(j, 1)
    def test_read_3(self):
        s = "999999 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  999999)
        self.assertEqual(j, 1)
    def test_read_4(self): 
        s = "999999 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  999999)
        self.assertEqual(j, 999999)
    def test_read_5(self):
        s = "999999 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  999999)
        self.assertEqual(j, 1)
    def test_read_6(self):
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 1)
    def test_read_7(self):
        s = "30202 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  30202)
        self.assertEqual(j, 1)
    def test_read_8(self):
        s = "10 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 10)





    # ----
    # eval
    # ----

    def test_eval_1(self): #max cycle length #three more reads, three more test prints and three more solves
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20) #change the 1s

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125) #fixed

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89) #fixed

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174) #fixed

    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_6(self):
        v = collatz_eval(10, 10)
        self.assertEqual(v, 7) #fixed

    def test_eval_7(self):
        v = collatz_eval(999999, 1)
        self.assertEqual(v, 525) #fixed

    def test_eval_8(self):
        v = collatz_eval(999999, 1)
        self.assertEqual(v, 525) #fixed

    def test_eval_9(self):
        v = collatz_eval(999999, 999999)
        self.assertEqual(v, 259) #fixed
    # -----
    # print
    # -----

    def test_print(self): #"2 more tests that work with correct answer" 
        w = StringIO() # copy and put other inputs #running coverage = unit tests
        collatz_print(w, 1, 10, 20) 
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print2(self):
        w = StringIO() # copy and put other inputs #running coverage = unit tests
        collatz_print(w, 100, 200, 125) 
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print3(self):
        w = StringIO() # copy and put other inputs #running coverage = unit tests
        collatz_print(w, 8888, 32, 262) 
        self.assertEqual(w.getvalue(), "8888 32 262\n")

    def test_print4(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1) 
        self.assertEqual(w.getvalue(), "1 1 1\n")

    def test_print5(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1) 
        self.assertEqual(w.getvalue(), "1 1 1\n")

    def test_print6(self):
        w = StringIO()
        collatz_print(w, 999999, 999999, 259) 
        self.assertEqual(w.getvalue(), "999999 999999 259\n")

    def test_print7(self):
        w = StringIO()
        collatz_print(w, 999999, 1, 525) 
        self.assertEqual(w.getvalue(), "999999 1 525\n")
    
    def test_print8(self):
        w = StringIO()
        collatz_print(w, 10, 10, 7) 
        self.assertEqual(w.getvalue(), "10 10 7\n")
    # -----
    # solve
    # -----
    def test_solve(self): #2 more test solves" print the answer with correct formatting
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n") #change 1 to actual answer
    def test_solve2(self):
        r = StringIO("10 2\n999 200\n9999 43\n9 12\n") 
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 2 20\n999 200 179\n9999 43 262\n9 12 20\n") #fixed
    def test_solve3(self):
        r = StringIO("999999 999999\n999999 1\n2333 50000\n67483 777727\n1 1\n") 
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "999999 999999 259\n999999 1 525\n2333 50000 324\n67483 777727 509\n1 1 1\n") 
    def test_solve4(self):
        r = StringIO("999999 999999\n999999 1\n2333 50000\n67483 777727\n1 1\n") 
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "999999 999999 259\n999999 1 525\n2333 50000 324\n67483 777727 509\n1 1 1\n") 
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
