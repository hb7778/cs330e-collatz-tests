#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # 3 more tests for collatz_read()
    def test_read_1(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 200)
    def test_read_2(self):
        s = "9998 9999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  9998)
        self.assertEqual(j, 9999)
    def test_read_3(self):
        s = "47 47\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  47)
        self.assertEqual(j, 47)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # 3 more tests for collatz_eval()
    def test_eval_5(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)
    def test_eval_6(self):
        v = collatz_eval(47, 47)
        self.assertEqual(v, 105)
    def test_eval_7(self):
        v = collatz_eval(9998, 9999)
        self.assertEqual(v, 92)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # 3 more tests for collatz_print()
    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 10, 1, 20)
        self.assertEqual(w.getvalue(), "10 1 20\n")
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 47, 47, 105)
        self.assertEqual(w.getvalue(), "47 47 105\n")
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 9998, 9999, 92)
        self.assertEqual(w.getvalue(), "9998 9999 92\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # 3 more cases for collatz_solve()
    def test_solve_1(self):
        r = StringIO("1 1\n2 2\n111 111\n9999 9998\n1 9999\n700 7\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n2 2 2\n111 111 70\n9999 9998 92\n1 9999 262\n700 7 145\n")
    def test_solve_2(self):
        r = StringIO("4 444\n8 4\n836 800\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "4 444 144\n8 4 17\n836 800 135\n")
    def test_solve_3(self):
        r = StringIO("12 13\n7 1\n76 9999\n123 321\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "12 13 10\n7 1 17\n76 9999 262\n123 321 131\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
